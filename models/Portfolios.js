var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PortfolioSchema = new Schema({
    attributes: Object,
    LastModifiedDate: String,
    IsDeleted:Boolean,
    PageNumber__c:String,
    Name__c:String,
    Name:String,
    SystemModstamp:String,
    FolderType__c:String,
    OwnerId:String,
    CreatedById:String,
    RecordTypeId:String,
    IsSpecialMeeting__c:Boolean,
    Id:String,
    LastModifiedById:String
}, {collection: 'Portfolio'});

PortfolioSchema.attributes = {
    type: String,
    url: String
}

var Portfolio = mongoose.model('Portfolio', PortfolioSchema);
module.exports = Portfolio;
