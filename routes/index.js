var express = require("express");
var router = express.Router();
let Portfolio = require("../models/Portfolios.js");
const db = require("../db");

/* GET home page. */
router.get("/", function (req, res, next) {
  Portfolio.find({}).exec(function (err, docs) {
      if (err) return next(err);
      try {
          console.log("returned data:",docs);
          res.render("index", { data: docs,title: "Data"});
          // res.render("index", { title: "Data" });
          //res.send(docs);
      } catch (e) {
          return next(e);
      }
    });
});

router.post("/portfolio", (req, res, next) => {
  // //Return all portfolios
    // Portfolio.find({}).exec(function (err, docs) {
    //   if (err) return next(err);
    //   try {
    //       console.log("returned data:",docs);
    //       res.send(docs);
    //   } catch (e) {
    //       return next(e);
    //   }
    // });

  //Insert the new Json with the data.
  const form = req.body;
  if(!Array.isArray(req.body)) return next(new Error('Invalid data format'))
  Promise.all(req.body.map(form=>{
    return new Promise((resolve, reject)=>{
      (new Portfolio(form)).save((err, doc)=>{
        if(err) return reject(err);
        console.log(doc);
        return resolve(doc);
      })
    })
  })).then(docs=>{
    res.json({payload: docs});
    res.render("index", { data: docs });
  }).catch(e=>next(e))
});

module.exports = router;
